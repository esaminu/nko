var countdown = new ReactiveCountdown(30);
countdown.start(function(){
    // Change user's turn.
    setTimeout(Meteor.call('switchTurns'), 1000);
    countdown.start();
});

Template.storylist.helpers({
  stories: function () {
    return Stories.find();
  },
  getSubString: function(String) {
    return String.substring(0,140);
  }
});

Template.storyPage.helpers({
  comments: function() {
    return Comments.find({postId:this._id});
  },
  username: function(id){
    var user = Meteor.users.findOne({_id: id});
    return user.username;
  }
});

  Template.newpost.helpers({
      connection_status: function(){
          var user = Room.findOne({userId: Meteor.userId()});
          try{
              return user.status;
          } catch(TypeError){
              return "There was an error during connection. " +
                     "If this message still appears after 1 minute, please refresh the page or try signing in again.";
          }
      },
      connected: function(connection_status){
        if(connection_status == "Connected"){
            return true;
        }
        return false;
      },
      waiting: function(connection_status){
        if(connection_status == "Waiting") {
            return true;
        }
        return false;
      },

      isThisUsersTurn: function(){
        if(TempStories.findOne({$or: [{user1: Meteor.userId()}, {user2: Meteor.userId()}]}).userTurn == Meteor.userId()){
            return true;
        }
        return false;
      },

      story: function(){
          try{
              return TempStories.findOne({$or: [{user1: Meteor.userId()}, {user2: Meteor.userId()}]}).content;
          } catch(TypeError){
              // Handle type error here.
          }

      },

      turn_indicator: function(){
          if(TempStories.findOne({$or: [{user1: Meteor.userId()}, {user2: Meteor.userId()}]}).userTurn == Meteor.userId()){
              return "It's your turn, you may edit the story.";
          } else {
              return "It's the other user's turn, please wait until they are done editing the story.";
          }
      },

      getTurnCountdown: function(){
          return countdown.get();
      }
  });

  // Event listeners
  Template.header.events({
      "click .new-post": function(){
          Router.go("/newpost");
      }
  });

  Template.storylist.events({
      "click #upvote-button": function () {
          if (Meteor.userId() != null) {
              var upCondition = Upvotes.findOne({postId: this._id, userId: Meteor.userId()}) == null;
              if (upCondition) {
                  Stories.update({_id: this._id}, {$inc: {upvotes: 1}});
                  Upvotes.insert({userId: Meteor.userId(), postId: this._id});
              }
          } else {
              alert("You need to be logged in in order to upvote this story.");
          }
      }
  });

  Template.newpost.events({
    "submit #story-form": function(event){
        event.preventDefault();

        var text = event.target.newbit.value;
        if(text != ""){
            if(TempStories.findOne({$or: [{user1: Meteor.userId()}, {user2: Meteor.userId()}]}).userTurn != Meteor.userId()){
                //alert("Please wait for the other user's turn to end.");
            } else {
                Meteor.call('updateTemporaryStory', text);
                event.target.newbit.value = "";
            }
        }
    },

    "click #post-button": function(){
        var title = prompt("You chose to post. please enter a title for this story: ");
        Meteor.call('postStory', title);
    },

    "change #user-timer": function(){
        console.log("Turn timer has changed.");
    }
  });

  // Rendered callbacks
  Template.newpost.onRendered(function(){
      if(!this.rendered){
        Meteor.call('joinRoom', function(error, response){
            if(response != undefined){
                alert(response);
            }
        });
      }
  });

Template.storyPage.events({
  "click #upvote-button": function(){
    if(Meteor.userId()!=null){
      var upCondition = Upvotes.findOne({postId: this._id,userId:Meteor.userId()}) == null;
      if( upCondition ) { 
        Stories.update({_id : this._id},{$inc:{upvotes : 1}});
        Upvotes.insert({userId:Meteor.userId(),postId:this._id});
      }    
    } else {
      alert("You need to be logged in in order to upvote this story.");
    }
  },
  "click #upvote-button-comments": function(){
    if(Meteor.userId()!=null){
      var upCondition = Upvotes.findOne({postId: this._id,userId:Meteor.userId()}) == null;
      if( upCondition ) { 
        Comments.update({_id : this._id},{$inc:{upvotes : 1}});
        Upvotes.insert({userId:Meteor.userId(),postId:this._id});
      }    
    } else {
      alert("You need to be logged in in order to upvote this story.");
    }
  },
  "click #add-comment": function(){
    if(Meteor.userId()!=null){
      var commenText = document.getElementById("comment-text").value;
      Comments.insert({userId: Meteor.userId(),postId:this._id,content:commenText,createdAt:new Date(), upvotes:0}); 
      document.getElementById("comment-text").value = '';   
    } else {
      alert("You need to be logged in in order to comment.");
    }
  }
});


Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY'
});


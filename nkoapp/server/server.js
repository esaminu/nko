Meteor.startup(function () {
    if (Stories.find().count() === 0) {
        Stories.insert({
            title: 'Lorem ipsum',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor quam ut lacus sagittis fringilla. Maecenas vel est porttitor, bibendum ipsum a, posuere est. Fusce ultrices odio gravida nunc dapibus, et congue quam porta. Integer egestas ut justo quis pulvinar. Nam ornare quam ut dignissim gravida. Suspendisse in condimentum nulla. Maecenas semper ligula id massa dignissim, vitae pellentesque risus condimentum. Proin nisi mi, ornare id malesuada ut, egestas quis risus. Aenean vitae pulvinar nibh. Maecenas sollicitudin ut justo vel pulvinar. Suspendisse sed aliquet ligula. Mauris cursus molestie sollicitudin. Curabitur ornare semper tortor at iaculis.',
            upvotes: 0,
            createdAt: new Date()
        });
        Stories.insert({
            title: 'Fusce ut',
            content: 'Fusce ut dignissim libero. Suspendisse eu fringilla arcu. Quisque semper ipsum arcu, id vestibulum justo venenatis rutrum. Aliquam erat volutpat. Nullam quis congue felis, at varius dui. Nulla ultricies ligula eget semper suscipit. Aliquam eget rhoncus dolor. Curabitur venenatis nunc ac tellus interdum consectetur. In hac habitasse platea dictumst. Nunc mattis fringilla purus feugiat luctus.',
            upvotes: 0,
            createdAt: new Date()
        });
        Stories.insert({
            title: 'Sed ornare',
            content: 'Sed ornare, velit a bibendum condimentum, nunc neque finibus mi, ac volutpat justo quam vel nunc. Nunc dui libero, aliquam non laoreet ut, tempus ac nisi. Fusce mattis turpis a eleifend viverra. Maecenas at vestibulum libero. Sed ultrices, sapien eget semper egestas, velit ante ultrices sem, eu placerat augue nibh eget massa. Morbi imperdiet nibh nec libero maximus imperdiet. Fusce ultrices neque vel urna dapibus, eget tincidunt ex fringilla.',
            upvotes: 0,
            createdAt: new Date()
        });
    }
    if (Comments.find().count() === 0) {
        Comments.insert({
            userId: 'mo499qi6ZRHAJz5JX',
            postId: 'QuHxGKBJBgD7aShsL',
            content: 'Sathia sutta la payasum',
            upvotes: 0,
            createdAt: new Date()
        });
    }
});

Meteor.methods({
    joinRoom: function () {
        /* Query the Room collection to see if there are available users. If there are,
         * pair the user immediately with a user who's available. If there are no available users,
         * add the user into the room and set them as available. */
        var availableUsers = Room.find({available: true}).count();
        if (availableUsers > 0) {
            if (Meteor.call('pairUsers', this.userId)) {
                return "We have managed to connect you with someone. You may start typing!";
            }
        } else {
            Room.insert({userId: this.userId, available: true, status: 'Waiting'});
            console.log("User with id " + this.userId + " has joined the room.");
            return "Added to waiting room.";
        }
    },

    pairUsers: function (userId) {
        /* Query the Room collection for available user and their ids. Pick a random available
         user and pair them with the current user. After that, create a document in the TempStory collection
         that will allow these 2 users to collaborate on a story together. THe document should state these 2
         users as participants. */

        // Add the user to the room if they are not in the room already.
        if (Room.find({$and:[{userId: userId}, {available: true}]}).count() == 0) {
            Room.insert({userId: userId, available: true, status: 'Waiting'});
            var randomUser = Room.findOne({available: true});
            if (TempStories.insert({user1: userId, user2: randomUser.userId, content: "", userTurn: userId, turnCountdown: 0, postCountdown: 0})) {
                Room.update({userId: userId}, {$set: {available: false, status: 'Connected'}});
                Room.update({userId: randomUser.userId}, {$set: {available: false, status: 'Connected'}});
                return true;
            }
        }
        return false
    },

    updateTemporaryStory: function (text) {
        // Update content of temporary stories collection document.
        var tempStory = TempStories.findOne({$or: [{user1: this.userId}, {user2: this.userId}]});
        var tempStoryContent = tempStory.content;
        tempStoryContent += " " + text;
        // Update who's user's turn it is.
        if(tempStory.userTurn == tempStory.user1){
            TempStories.update({$or: [{user1: this.userId}, {user2: this.userId}]},
                {$set: {content: tempStoryContent, userTurn: tempStory.user2}});
        } else {
            TempStories.update({$or: [{user1: this.userId}, {user2: this.userId}]},
                {$set: {content: tempStoryContent, userTurn: tempStory.user1}});
        }
    },

    postStory: function (title) {

        var tempStory = TempStories.findOne({$or: [{user1: this.userId}, {user2: this.userId}]});

        // Add the story to the Stories collection.
        Stories.insert({title: title, content: tempStory.content, upvotes: 0, createdAt: new Date()});
        // Delete the story from the TempStories collection.
        TempStories.remove({$or: [{user1: this.userId}, {user2: this.userId}]});
        // Remove the users from the room.
        removeUsersFromRoom(tempStory.user1, tempStory.user2);
    },

    switchTurns: function(){
        var tempStory = TempStories.findOne({$or: [{user1: this.userId}, {user2: this.userId}]});
        if(tempStory.userTurn == tempStory.user1){
            TempStories.update({$or: [{user1: this.userId}, {user2: this.userId}]},
                {$set: {userTurn: tempStory.user2}});
        } else {
            TempStories.update({$or: [{user1: this.userId}, {user2: this.userId}]},
                {$set: {userTurn: tempStory.user1}});
        }
    },

    removeUsersFromRoom: function(user1, user2){
        Room.remove({userId: user1});
        Room.remove({userId: user2});
    }

});
Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound',
});

Router.route('/', {name: 'storylist'});
Router.route('/newpost', {name: 'newpost'})

Router.route('/stories/:_id', {
	name: 'storyPage',
	data: function() { return Stories.findOne(this.params._id); }
});

